package edu.unk.fun330.controllers;

import edu.unk.fun330.*;
import edu.unk.fun330.base.*;

import java.util.Iterator;

public class EmelieController extends ShipController {

    private float pi = 3.141592653589793F;
    private boolean hasAdjusted = false;
    private boolean shield = false;

    public EmelieController(Ship ship) {
        super(ship);
    }

    @Override
    public String getName() {
        return "Turtle";
    }

    @Override
    public ControllerAction makeMove(Universe u) {
        FlightAdjustment fa = new FlightAdjustment();
        FlyingObject closestShip = null;
        FlyingObject closestObject = null;
        FlyingObject closestBonus = null;

        float closestObjectDistance = 3.4028235E38F;
        float closestShipsDistance = 3.4028235E38F;
        float closestBonusDistance = 3.4028235E38F;
        float angle;
        Iterator var10 = u.getFlyingObjects().iterator();

        while (var10.hasNext()) {
            FlyingObject flyingObject = (FlyingObject) var10.next();

            if (!this.ship.equals(flyingObject)) {
                float distance = Util.distance(this.ship.getX(), this.ship.getY(), flyingObject.getX(), flyingObject.getY());
                if (flyingObject instanceof Bonus && !(flyingObject instanceof BombBonus) && distance < closestBonusDistance) {
                    closestBonusDistance = distance;
                    closestBonus = flyingObject;
                }
                if (flyingObject instanceof Ship && distance < closestObjectDistance) {
                    closestShipsDistance = distance;
                    closestShip = flyingObject;
                }
                if (distance < closestObjectDistance) {
                    closestObjectDistance = distance;
                    closestObject = flyingObject;
                }
            }
        }

        if (closestObject instanceof Bullet && closestObjectDistance < 40.0F && !this.ship.isShieldUp()) {
            if(this.ship.canShield()) {
                shield = true;
                return new ToggleShield();
            }
        } else if (closestShip != null && closestShipsDistance < 230.0F) {
            fa.setFacing((pi*2F) - Util.findAngle(this.ship.getX(), this.ship.getY(), closestShip.getNextX((int) (closestShipsDistance /10)), closestShip.getNextY((int) (closestShipsDistance /10))) +(pi/2F));
            fa.setAcceleration(.5F);
            if(!hasAdjusted) {
                hasAdjusted = true;
                return fa;
            } else if(this.ship.canFireEMP()) {
                return new FireEMP();
            } else if(this.ship.canFireLaser()) {
                return new FireLaser();
            } else if(this.ship.canFire()) {
                return new FireBullet();
            } else if(this.ship.canTeleport() && closestBonus != null) {
                return new ShipJump(closestBonus.getX(), closestBonus.getY());
            } else if(shield) {
                shield = false;
                return new ToggleShield();
            }
        } else if (closestObject instanceof BombBonus || closestObject instanceof BlackHole) {
            if(!hasAdjusted) {
                hasAdjusted = true;
                shield = false;
                angle = closestObject.getHeading() - pi /2.0F;
                fa.setFacing(angle);
                fa.setAcceleration(-0.5F);
                return fa;
            } else if(this.ship.canShield() && closestObject instanceof BombBonus) {
                shield = true;
                return new ToggleShield();
            }
        } else if(closestBonus != null) {
            if(closestBonus instanceof FuelBonus && this.ship.getFuelAmmount()<200) {
                fa.setFacing(Util.calcAngle(this.ship.getX(), this.ship.getY(), closestBonus.getX(), closestBonus.getY()));
                fa.setAcceleration(.5F);
                return fa;
            }
            if(closestBonus instanceof BulletBonus && this.ship.getBulletsAmmount()<10) {
                fa.setFacing(Util.calcAngle(this.ship.getX(), this.ship.getY(), closestBonus.getX(), closestBonus.getY()));
                fa.setAcceleration(.5F);
                return fa;
            }
            if(this.ship.canTeleport() && !(closestBonus instanceof BlackHole) && !(closestBonus instanceof BombBonus)) {
                return new ShipJump(closestBonus.getX(), closestBonus.getY());
            }
            else {
                fa.setFacing(Util.calcAngle(this.ship.getX(), this.ship.getY(), closestBonus.getX(), closestBonus.getY()));
                fa.setAcceleration(.4F);
                return fa;
            }
        }
        return fa;
    }
}