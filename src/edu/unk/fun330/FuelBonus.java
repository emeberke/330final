package edu.unk.fun330;

import java.awt.*;


public class FuelBonus extends Bonus {

	public static Image fuelBonusImage;
	
	public FuelBonus(float x, float y, int amount) {
		super(x, y, amount);
	}

	public Color getColor() { return Constants.fuelColor; }
	public Image getImage() {return fuelBonusImage;};

}
