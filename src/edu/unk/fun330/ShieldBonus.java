package edu.unk.fun330;

import java.awt.*;


public class ShieldBonus extends Bonus {
	
	public static Image shieldBonusImage;

	public ShieldBonus(float x, float y, int amount) {
		super(x, y, amount);
	}

	public Color getColor() { return Constants.shieldColor; }
	public Image getImage() {return shieldBonusImage;};

}
