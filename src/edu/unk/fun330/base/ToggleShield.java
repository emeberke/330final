package edu.unk.fun330.base;

/**
 * Toggle shield action returned by controllers from their makeMove method.
 * Turns the shield off if it was on, and on if it was off (and the ship canShield).
 * 
 * @author jdh
 *
 */
public class ToggleShield extends ControllerAction { }
