package edu.unk.fun330.base;

import edu.unk.fun330.Ship;
import edu.unk.fun330.Universe;

public abstract class ShipController {
	
	protected final Ship ship;
	
	public ShipController(Ship ship){
		this.ship = ship;
		//ship.setShipController(this);  //changed 11-18-2019...moved up into engine
	}

	public abstract String getName();
	
	// *** Should modify this to not pass the entire universe into the controllers!!!
	public abstract ControllerAction makeMove(Universe gd);

	public Ship getShip() { return ship; }
}
