package edu.unk.fun330;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

/**
 * Parent class of all game FlyingObjects. Bonus and Ship are subclasses.
 * 
 * @author hastings
 *
 */
public abstract class FlyingObject {

	protected float facing;
	protected float radius;

	protected float heading;
	protected float speed;
	protected float acceleration;

	protected float x;
	protected float y;

	public FlyingObject(float x, float y) {
		this.x = x;
		this.y = y;

		facing = 0;
		heading = 0;
		speed = 0;
		radius = 0;
	}

	/** Have the object react to being hit by another flying object.
	 * Return true if the object should be removed from the universe after being hit.
	 */
	protected abstract boolean hitBy(FlyingObject fo, Universe u);
	
	protected abstract Color getColor();
	public abstract float getRadius();
	protected Image getImage() {return null;};
	
	protected void setRadius(float r) {this.radius = r; }
	public abstract void paint(Graphics g);

	public float getFacing() { return facing; }
	public float getHeading() { return heading; }
	public float getSpeed() { return speed; }
	public float getX() { return x; }
	public float getY() { return y; }
	protected void setFacing(float facing) { this.facing = facing; }
	protected void setHeading(float heading) { this.heading = heading; }
	protected void setSpeed(float speed) { this.speed = speed; }
	protected void setX(float x) { this.x = x; }
	protected void setY(float y) { this.y = y; }
	
	/**
	 * Calculate the X coordinate of the object for the next display frame.
	 * @return Next X position
	 */
	public float getNextX() { return  x + speed * (float) Math.cos(heading); }
	
	/**
	 * Calculate the Y coordinate of the object for the next display frame.
	 * @return Next Y position
	 */
	public float getNextY() { return  y + speed * (float) Math.sin(heading); }
	
	/**
	 * Calculate the estimated X coordinate of the object after <b>i</b> frames.
	 * @param i Number of elapsed frames
	 * @return Estimated X position
	 */
	public float getNextX(int i) { return  x + speed * i * (float) Math.cos(heading); }
	
	/**
	 * Calculate the estimated Y coordinate of the object after <b>i</b> frames.
	 * @param i Number of elapsed frames
	 * @return Estimated Y position
	 */
	public float getNextY(int i) { return  y + speed * i * (float) Math.sin(heading); }
	public float getAcceleration() { return acceleration; }
	protected void setAcceleration(float acceleration) { this.acceleration = acceleration; }
	
	protected boolean offLeftUniverse() { return (x-radius < 0); }
	protected boolean offRightUniverse() { return (x+radius> Universe.WIDTH); }
	protected boolean offBottomUniverse() { return (y-radius<0); }
	protected boolean offTopUniverse() { return (y+radius>Universe.HEIGHT); }
	protected boolean offUniverse() {
		return (offLeftUniverse() || offRightUniverse() || offBottomUniverse() || offTopUniverse());
	}
	
	/**
	 * Return true if the object should be removed for leaving the universe.
	 * @param gd
	 * @return
	 */
	protected abstract boolean handleOffUniverse(Universe gd);
	/*
		if (offUniverse()){
			if(this instanceof Ship)
				Constants.logWriter.logShipDeath((Ship)this,(FlyingObject)null);
			//gd.getFlyingObjects().removeElement(this);
			return true;
		}
		return false;
	}
	*/
	
	private float distance (FlyingObject fo) {
		return Util.distance(this.getNextX(), this.getNextY(), fo.getNextX(), fo.getNextY() );
	}
	
	/**
	 * Return true if the object has collided with the FlyingObject.
	 * @param fo
	 * @return
	 */
	protected boolean intersects (FlyingObject fo) {
		float distance = this.distance(fo);
		//If there is a collision
		return (distance < this.getRadius() + fo.getRadius());
	}

}
