package edu.unk.fun330;

import java.awt.Color;
import java.awt.Image;

public class LaserBonus extends Bonus {

	public static Image laserBonusImage;
	
	public LaserBonus(float x, float y, int amount) {
		super(x, y, amount);
	}

	public Color getColor() {return Constants.laserColor; }
	public Image getImage() {return laserBonusImage;};
	
}
