package edu.unk.fun330;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import edu.unk.fun330.Universe;
import edu.unk.fun330.base.*;
import edu.unk.fun330.controllers.EmelieController;

import java.awt.*;

class TurtleControllerTest {
    Universe u;
    Ship ship;
    Ship ship2;
    BlackHole blackHole;
    BombBonus bombBonus;
    BulletBonus bulletBonus;
    EmpBonus empBonus;
    FuelBonus fuelBonus;
    LaserBonus laserBonus;
    LifeBonus lifeBonus;
    MagnetBonus magnetBonus;
    PointsBonus pointsBonus;
    ShieldBonus shieldBonus;
    ShipJumpBonus shipJumpBonus;
    ShipController controller;
    ControllerAction action;
    float pi = 3.141592653589793F;

    @BeforeAll
    static void beforeAll() { } // Run once before all test methods

    /**
     * Run before each of the test methods to configure the environment.
     * NOTE: the order of execution of the test cases is not predetermined
     */
    @BeforeEach
    void beforeEach() {
        System.out.println("Before each test method");
        // Configure the base environment for the test cases
        // Create universe, add ship to it, and link controller to ship
        u = new Universe();
        ship = new Ship(100,100,0);  // ship tied to this controller under test
        u.add(ship);
        controller = new EmelieController(ship);
        ship.setShipController(controller);
        ship2 = new Ship(50,50,1);  // second ship for testing
        u.add(ship2);
        blackHole = new BlackHole(1200,0, u); //x
        u.add(blackHole);
        bombBonus = new BombBonus(250,250,1); //x
        u.add(bombBonus);
        bulletBonus = new BulletBonus(700,200,1); //x
        u.add(bulletBonus);
        empBonus = new EmpBonus(300,1000,1); //x
        u.add(empBonus);
        fuelBonus = new FuelBonus(400,800,1); //x
        u.add(fuelBonus);
        laserBonus = new LaserBonus(500,500,1); //x
        u.add(laserBonus);
        lifeBonus = new LifeBonus(600,50,1); //x
        u.add(lifeBonus);
        magnetBonus = new MagnetBonus(700,700,1); //x
        u.add(magnetBonus);
        pointsBonus = new PointsBonus(800,200,1);
        u.add(pointsBonus);
        shieldBonus = new ShieldBonus(100,900,1); //x
        u.add(shieldBonus);
        shipJumpBonus = new ShipJumpBonus(1000,1000,1); //x
        u.add(shipJumpBonus);

    }

    @AfterEach
    void afterEach() { } // Run after each of the test methods

    @AfterAll
    static void afterAll() { } // Run once after all test methods have completed

    @Test
    void testFirstMoveRightofEnemy() {
        System.out.println("Testing first move with ship to the right of an enemy");
        action = controller.makeMove(u);
        assertTrue(action instanceof FlightAdjustment);

        FlightAdjustment fa = (FlightAdjustment)action; //expected flight adjustment
        assertEquals(fa.getFacing(), (pi*2F) - Util.findAngle(this.ship.getX(), this.ship.getY(), ship2.getNextX((int) (Util.distance(this.ship.getX(), this.ship.getY(), ship2.getNextX(), ship2.getNextY()) /10)),
                ship2.getNextY((int) (Util.distance(this.ship.getX(), this.ship.getY(), ship2.getNextX(), ship2.getNextY()) /10))) +(pi/2F));
        assertEquals(fa.getAcceleration(),.5f);
    }

    @Test
    void testFirstMoveLeftofEnemy() {
        System.out.println("Testing first move with ship to the left of an enemy");
        ship.setX(25);
        action = controller.makeMove(u);
        assertTrue(action instanceof FlightAdjustment);

        FlightAdjustment fa = (FlightAdjustment)action; //expected flight adjustment
        assertEquals(fa.getFacing(), (pi*2F) - Util.findAngle(this.ship.getX(), this.ship.getY(), ship2.getNextX((int) (Util.distance(this.ship.getX(), this.ship.getY(), ship2.getNextX(), ship2.getNextY()) /10)),
                ship2.getNextY((int) (Util.distance(this.ship.getX(), this.ship.getY(), ship2.getNextX(), ship2.getNextY()) /10))) +(pi/2F));
        assertEquals(fa.getAcceleration(),.5f);
    }

    @Test
    void testSecondMove() {
        action = controller.makeMove(u);
        System.out.println("Testing second move");
        action = controller.makeMove(u);
        assertTrue(action instanceof FireLaser); // second action
    }

    @Test
    void testAvoidBullet() {
        ship.setX(50);
        ship.setY(900);
        action = controller.makeMove(u);
        assertTrue(action instanceof FlightAdjustment);
    }

    @Test
    void testAvoidBlackHole() {
        ship.setX(1200);
        ship.setY(130);
        action = controller.makeMove(u);
        float angle = blackHole.getHeading() - pi / 2.0F;
        assertTrue(action instanceof FlightAdjustment);
        FlightAdjustment fa = (FlightAdjustment)action;
        assertEquals(fa.getFacing(), angle);
        assertEquals(fa.getAcceleration(), -.5f);
    }

    @Test
    void testAvoidBombBonus() {
        ship.setX(150);
        ship.setY(250);
        action = controller.makeMove(u);
        float angle = bombBonus.getHeading() - pi / 2.0F;
        assertTrue(action instanceof FlightAdjustment);
        FlightAdjustment fa = (FlightAdjustment)action;
        assertEquals(fa.getFacing(), angle);
        assertEquals(fa.getAcceleration(), -.5f);
        action = controller.makeMove(u);
        assertTrue(action instanceof ToggleShield);
    }

    @Test
    void testGetFuel() {
        ship.setX(400);
        ship.setY(700);
        action = controller.makeMove(u);
        assertTrue(action instanceof FlightAdjustment);
        FlightAdjustment fa = (FlightAdjustment)action;
        assertEquals(fa.getFacing(), Util.calcAngle(this.ship.getX(), this.ship.getY(), fuelBonus.getX(), fuelBonus.getY()));
        assertEquals(fa.getAcceleration(), .4f);
    }

    @Test
    void testGetEmp() {
        ship.setX(300);
        ship.setY(900);
        action = controller.makeMove(u);
        assertTrue(action instanceof FlightAdjustment);
        FlightAdjustment fa = (FlightAdjustment)action;
        assertEquals(fa.getFacing(), Util.calcAngle(this.ship.getX(), this.ship.getY(), empBonus.getX(), empBonus.getY()));
        assertEquals(fa.getAcceleration(), .4f);
    }

    @Test
    void testGetBulletBonus() {
        ship.setX(600);
        ship.setY(200);
        action = controller.makeMove(u);
        assertTrue(action instanceof FlightAdjustment);
        FlightAdjustment fa = (FlightAdjustment)action;
        assertEquals(fa.getFacing(), Util.calcAngle(this.ship.getX(), this.ship.getY(), bulletBonus.getX(), bulletBonus.getY()));
        assertEquals(fa.getAcceleration(), .4f);
    }

    @Test
    void testGetLaserBonus() {
        ship.setX(500);
        ship.setY(400);
        action = controller.makeMove(u);
        assertTrue(action instanceof FlightAdjustment);
        FlightAdjustment fa = (FlightAdjustment)action;
        assertEquals(fa.getFacing(), Util.calcAngle(this.ship.getX(), this.ship.getY(), laserBonus.getX(), laserBonus.getY()));
        assertEquals(fa.getAcceleration(), .4f);
    }

    @Test
    void testGetLifeBonus() {
        ship.setX(500);
        ship.setY(50);
        action = controller.makeMove(u);
        assertTrue(action instanceof FlightAdjustment);
        FlightAdjustment fa = (FlightAdjustment)action;
        assertEquals(fa.getFacing(), Util.calcAngle(this.ship.getX(), this.ship.getY(), lifeBonus.getX(), lifeBonus.getY()));
        assertEquals(fa.getAcceleration(), .4f);
    }

    @Test
    void testGetMagnetBonus() {
        ship.setX(600);
        ship.setY(700);
        action = controller.makeMove(u);
        assertTrue(action instanceof FlightAdjustment);
        FlightAdjustment fa = (FlightAdjustment)action;
        assertEquals(fa.getFacing(), Util.calcAngle(this.ship.getX(), this.ship.getY(), magnetBonus.getX(), magnetBonus.getY()));
        assertEquals(fa.getAcceleration(), .4f);
    }

    @Test
    void testGetPointsBonus() {
        ship.setX(700);
        ship.setY(200);
        action = controller.makeMove(u);
        assertTrue(action instanceof FlightAdjustment);
        FlightAdjustment fa = (FlightAdjustment)action;
        assertEquals(fa.getFacing(), Util.calcAngle(this.ship.getX(), this.ship.getY(), pointsBonus.getX(), pointsBonus.getY()));
        assertEquals(fa.getAcceleration(), .4f);
    }

    @Test
    void testGetShieldBonus() {
        ship.setY(800);
        action = controller.makeMove(u);
        assertTrue(action instanceof FlightAdjustment);
        FlightAdjustment fa = (FlightAdjustment)action;
        assertEquals(fa.getFacing(), Util.calcAngle(this.ship.getX(), this.ship.getY(), shieldBonus.getX(), shieldBonus.getY()));
        assertEquals(fa.getAcceleration(), .4f);
    }

    @Test
    void testGetShipJumpBonus() {
        ship.setX(900);
        ship.setY(1000);
        action = controller.makeMove(u);
        assertTrue(action instanceof FlightAdjustment);
        FlightAdjustment fa = (FlightAdjustment)action;
        assertEquals(fa.getFacing(), Util.calcAngle(this.ship.getX(), this.ship.getY(), shipJumpBonus.getX(), shipJumpBonus.getY()));
        assertEquals(fa.getAcceleration(), .4f);
    }

}
