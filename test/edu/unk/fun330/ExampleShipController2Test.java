package edu.unk.fun330;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import edu.unk.fun330.Universe;
import edu.unk.fun330.base.*;
import edu.unk.fun330.controllers.ExampleShipController2;

/**
 * This junit test class tests the ExampleShipController2 class.
 * 
 * @author hastings
 *
 */
class ExampleShipController2Test {

	Universe u;
	Ship ship;
	ShipController controller;
	ControllerAction action;

	@BeforeAll
	static void beforeAll() { } // Run once before all test methods

	/**
	 * Run before each of the test methods to configure the environment.
	 * NOTE: the order of execution of the test cases is not predetermined
	 */
	@BeforeEach
	void beforeEach() {
		System.out.println("Before each test method");
		// Configure the base environment for the test cases
		// Create universe, add ship to it, and link controller to ship
		u = new Universe();
		ship = new Ship(100,100,0);  // ship tied to this controller under test
		u.add(ship);
		controller = new ExampleShipController2(ship);

		// Add other objects to the test universe
		Ship ship2 = new Ship(50,50,1);  // second ship for testing
		u.add(ship2);
	}

	@AfterEach
	void afterEach() { } // Run after each of the test methods

	@AfterAll
	static void afterAll() { } // Run once after all test methods have completed

	/** Test the very first action by the controller for a ship to the right of a FlyingObject.
	 * Make sure it's a flight adjustment, and has the expected settings.
	 */
	@Test
	void testFirstMoveRightofEnemy() {
		System.out.println("Testing first move with ship to the right of an enemy");
		action = controller.makeMove(u);
		assertTrue(action instanceof FlightAdjustment); // first action
		FlightAdjustment fa = (FlightAdjustment)action; //expected flight adjustment
		assertEquals(fa.getFacing(),(float)Math.PI * .5f);
		assertEquals(fa.getAcceleration(),.05f);
	}

	/** Test the very first action by the controller for a ship to the left of a FlyingObject.
	 * Make sure it's a flight adjustment, and has the expected settings.
	 */
	@Test
	void testFirstMoveLeftofEnemy() {
		System.out.println("Testing first move with ship to the left of an enemy");
		ship.setX(25);
		action = controller.makeMove(u);
		assertTrue(action instanceof FlightAdjustment); // first action
		FlightAdjustment fa = (FlightAdjustment)action; //expected flight adjustment
		assertEquals(fa.getFacing(),(float)Math.PI * -.5f);
		assertEquals(fa.getAcceleration(),.05f);
	}

	/** Test the second action by the controller. This example controller always 
	 * fires a bullet as it's second action.
	 */
	@Test
	void testSecondMove() {
		action = controller.makeMove(u);
		System.out.println("Testing second move");
		action = controller.makeMove(u);
		assertTrue(action instanceof FireBullet); // second action
	}

}
